### Мониторинг приложения

1. Для работы нужно 4 виртуальные машины
   - ВМ самого приложения
   - ВМ gitlab-runner
   - ВМ prometheus
   - ВМ grafana

2. Перед запуском установить yc, terraform, ansible.

Yandex Cloud (CLI) — скачиваемое программное обеспечение для управления вашими облачными ресурсами через командную строку.
https://cloud.yandex.ru/docs/cli/quickstart#install

Получение OAuth-токена - https://cloud.yandex.ru/docs/iam/concepts/authorization/oauth-token

3. Заменить registration-token на правильный в файле skillbox-diploma-3-monitiring/infra/terraform/ansible/roles/gitlab-runner/tasks/main.yml

4. Настройка Terraform

Скопировать файл .terraformrc из директории provider-terraform/ в домашнюю директорию. (~/)

5. Сделать изменения.
- Изменить или добавить в Settings->CI/CD->Variables (ENV_VAR) переменную с именем SSH_PRIVATE_KEY на свой приватный (id_rsa) ключ.
- Добавить файлы в /terraform/variables.tf и /terraform/private.auto.tfvars со значениями внутри.

6. Заменить в ветке main по пути skillbox-diploma-3-monitiring/infra/terraform/ansible/roles/gitlab-runner/vars/main.yml
   параметр registration_token на правильный.

7. Заменить в ветке main по пути skillbox-diploma-3-monitiring/infra/terraform/ansible/roles/installation-prometheus/files/targets.json на IP адрес сервера приложения.
   
8. Перейти в директорию skillbox-diploma-3-monitiring/infra/terraform, сделать исполняемый, и запустить скрипт start_terraform.sh для запуска создания ВМ

9. Перед запуском pipeline заменить в .gitlab-ci.yml переменную REMOTE_IP на IP адрес сервера приложения.

### Про кэш и артифакты.
https://mcs.mail.ru/blog/sposoby-keshirovaniya-v-gitlab-ci-rukovodstvo-v-kartinkah

========================

## Запуск локально на компьютере
Чтобы скопировать репозиторий к себе для работы, вам нужно следовать [этим инструкциям](https://docs.github.com/en/github/creating-cloning-and-archiving-repositories/creating-a-repository-on-github/duplicating-a-repository#mirroring-a-repository-in-another-location).

## Runtime
Приложение отвечает по 3 эндпоинтам:  
* /health - 200 ok
* /metrics - в формате метрик для prometheus, включая счётчик запросов в основной эндпоинт `skillbox_http_requests_total`
* / - основной эндпоинт, возвращающий часть запроса и генерирующий строчку лога.

## Как работать с приложением без docker:  
1. Установить golang 1.16
2. Установить зависимости:

   ```shell
   go mod download
   ```

3. Запустить тесты:
   ```shell
   go test -v ./...
   ```
4. Собрать приложение:
   ```
   GO111MODULE=on go build -o app cmd/server/app.go
   ```
5. Запустить его:
   ```shell
   ./app
   ```

## Как работать с приложением в docker:  
1. Установить docker
2. Запустить тесты
   ```shell
   ./run-tests.sh
   ```
3. Собрать:
   ```shell
   docker-compose build
   ```
   or
   ```shell
   docker build . -t skillbox/app
   ```
4. Запустить:
   ```shell
   docker-compose up
   ```
   or
   ```shell
   docker run -p8080:8080 skillbox/app
   ```
